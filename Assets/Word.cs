﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Word {

    /// <summary>
    /// This is the word
    /// </summary>
    public readonly string wordName;

    [SerializeField]
    private List<StatisticData> statisticDatas = new List<StatisticData>();
    public List<StatisticData> StatisticDatas { get { return statisticDatas; } private set { statisticDatas = value; } }

    /// <summary>
    /// Generates new word with given name
    /// </summary>
    /// <param name="name"></param>
    public Word(string name)
    {
        wordName = name;
    }

    /// <summary>
    /// Returns the total number of times this word was shown.
    /// </summary>
    public int TimesShown {
        get
        {
            return StatisticDatas.Count;
        }
    }
    /// <summary>
    /// Returns the total number of yes ansvers.
    /// </summary>
    public int YesAnsvers {
        get
        {
            int yesAnsvers = 0;
            foreach (var data in StatisticDatas)
            {
                if (data.type == StatisticData.Type.Yes)
                    yesAnsvers++;
            }
            return yesAnsvers;
        }
    }
    /// <summary>
    /// Returns the total number of no ansvers
    /// </summary>
    public int NoAnsvers {
        get
        {
            int noAnsvers = 0;
            foreach (var data in StatisticDatas)
            {
                noAnsvers++;
            }
            return noAnsvers;
        }
    }
    /// <summary>
    /// Returns all date times as a list
    /// </summary>
    public List<DateTime> DateTimes {
        get
        {
            List<DateTime> dateTimes = new List<DateTime>();
            foreach (var statisticData in StatisticDatas)
            {
                dateTimes.Add(statisticData.dateTime);
            }
            return dateTimes;
        }
    }
    /// <summary>
    /// Returns all times between clicks as a list
    /// </summary>
    public List<float> TimesBetweenChoices {
        get
        {
            List<float> timesBetweenChoices = new List<float>();
            foreach (var statisticData in StatisticDatas)
            {
                timesBetweenChoices.Add(statisticData.timeUsedBeforeChoice);
            }
            return timesBetweenChoices;
        }
    }

    /// <summary>
    /// Adds entry to his word with given data returns true if succesfully added
    /// </summary>
    /// <param name="data"></param>
    public bool Entry_Add(StatisticData data)
    {
        if (data.timeUsedBeforeChoice > 0 && (data.type == StatisticData.Type.No || data.type == StatisticData.Type.Yes) && data.dateTime != null)
        {
            StatisticDatas.Add(data);
            return true;
        }
        else
            return false;
    }
}

[Serializable]
public class StatisticData
{
    public enum Type { Yes, No }
    public Type type;

    public DateTime dateTime;
    public float timeUsedBeforeChoice;

    public StatisticData(Type type, DateTime dateTime, float timeUsedBeforeChoice)
    {
        this.type = type;
        this.dateTime = dateTime;
        this.timeUsedBeforeChoice = timeUsedBeforeChoice;
    }
}
